Goal: To create a simple web app - Address book. A contact in the address book should have following attributes: First Name, Last Name, Phone number, Address. Client User should be able see FULL list of contacts, or part of the list filtered by First letter of a contact name. In case when list of contacts on a page exseeds particular number  (for example 25), it's required to have pagination. Number of contacts per page can be configurable.

Required Features: Create-Read-Update-Delete of contacts. An Index (Hiperlink Characters that loads contacts for particular Char). Setup number of contacts per page. Search by name, last name. Nice to have: User's interactions doesn't affect whole page (no page reloading), but client-server communication is implemented through AJAX requests to REST web services.
Security is out of scope.

DB: MySQL. Create a simple sql script that would populate DB with test data.

Expected results: compiled code that is ready to run in some Servlet/Application Container (Tomcat, WebLogic, etc.); source code that is ready to run in IDE and can be built by an Ant script or Maven.


-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 06 2015 г., 06:57
-- Версия сервера: 5.5.41-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `adressBook`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) COLLATE utf8_bin NOT NULL,
  `lastName` varchar(100) COLLATE utf8_bin NOT NULL,
  `phoneNumber` int(12) NOT NULL,
  `address` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `phoneNumber`, `address`) VALUES
(1, 'Laura', 'Kline', 126620086, '25 W 13th St Apt 5an'),
(2, 'Joyce', 'Collins', 127496104, '618 W 113th St Apt 3f'),
(3, 'Visitacion', 'Fuentes', 126620005, '55 Tiemann Pl'),
(4, 'Aurelia', 'Dearmas', 126620081, '220 Manhattan Ave Apt 4a'),
(5, 'Eric', 'Garfinkel', 126626375, '135 Manhattan Ave'),
(6, 'David', 'Garfinkel', 126620007, '135 Manhattan Ave'),
(7, 'Soonbin', 'Chung', 126620014, '320 Riverside Dr Apt 11'),
(8, 'Paul', 'Zimmerman', 126620049, '250 W 104th St Apt'),
(9, 'Benjamin', 'Goldstein', 126620049, '250 W 104th St'),
(10, 'Okanlawo', 'Coker', 324483068, '173 Minetta Rd Somerset'),
(11, 'Joe', 'Berger', 124480287, '400 W 43rd St Apt 33p'),
(12, 'Joseph', 'Carter', 138954300, '2431 W Grand Blvd'),
(13, 'Zeena', 'Kazangy', 138411010, '6061 W Vernor Hwy Ste 1'),
(14, 'Koepp', 'Norman', 138715489, '3011 W Grand Blvd Ste 871'),
(15, 'Garner', 'Toya', 138397900, '14521 E 7 Mile Rd'),
(16, 'Kim', 'Fleming', 138918088, '3704 E 8 Mile Rd'),
(17, 'Nadell', 'Sergent', 138918088, '3704 E 8 Mile Rd'),
(18, 'Ajay', 'Ojha', 126220086, '2700 Martin Luther King Jr Blvd'),
(19, 'Jeffery', 'Jaskolski', 126630086, '22151 Moross Rd Ste G34'),
(20, 'Michael', 'Szewczyk', 138424255, '7433 Michigan Ave'),
(21, 'Malaney', 'Brown', 138384880, '14755 Fenkell St'),
(22, 'Robert', 'Roy', 132715555, '19600 W Warren Ave'),
(23, 'Jordan', 'Millard', 132720118, '17563 Greenfield Rd Ste 1'),
(24, 'Alan', 'Josef', 138397310, '19150 Kelly Rd'),
(25, 'Milton', 'Campbell', 226620086, '7142 W 7 Mile Rd'),
(26, 'Dennis', 'Rogers', 135334333, '20429 W 7 Mile Rd'),
(27, 'Gabriel', 'Williams', 138737474, '3011 W Grand Blvd Ste 867');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package ua.poloz.service;

import java.util.List;

import ua.poloz.exception.UserNotFoundException;
import ua.poloz.model.Users;

public interface UserService {
	
	public Users create(Users sp);
	public Users get(Integer id);
	public List<Users> getAll();
	public Users update(Users sp) throws UserNotFoundException;
	public Users delete(Integer id) throws UserNotFoundException;
        public void initTestUsers();

}

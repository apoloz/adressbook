package ua.poloz.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.poloz.exception.UserNotFoundException;
import ua.poloz.model.Users;
import ua.poloz.repository.UserRepository;

@Service
@Transactional(rollbackFor=UserNotFoundException.class)
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public Users create(Users sp) {
		return userRepository.save(sp);
	}

	@Override
	public Users get(Integer id) {
		return userRepository.findOne(id);
	}

	@Override
	public List<Users> getAll() {
		return userRepository.findAll();
	}

	@Override
	public Users update(Users sp) throws UserNotFoundException {
		Users sUserToUpdate = get(sp.getId());
		if (sUserToUpdate == null)
			throw new UserNotFoundException(sp.getId().toString());
		sUserToUpdate.update(sp);
		return sUserToUpdate;
	}

	@Override
	public Users delete(Integer id) throws UserNotFoundException {
		Users sUser = get(id);
		if (sUser == null)
			throw new UserNotFoundException(id.toString());
		userRepository.delete(id);
		return sUser;
	}

        @Override
        public void initTestUsers(){
            List<Users> users = new ArrayList<>();
            Users user;
            user = new Users("Laura", "Kline", 126620086, "25 W 13th St Apt 5an");
            users.add(user);
            user = new Users("Joyce", "Collins", 127496104, "618 W 113th St Apt 3f");
            users.add(user);
            user = new Users("Visitacion", "Fuentes", 126620005, "55 Tiemann Pl");
            users.add(user);
            user = new Users("Aurelia", "Dearmas", 126620081, "220 Manhattan Ave Apt 4a");
            users.add(user);
            user = new Users("Eric", "Garfinkel", 126626375, "135 Manhattan Ave");
            users.add(user);
            user = new Users("David", "Garfinkel", 126620007, "135 Manhattan Ave");
            users.add(user);
            user = new Users("Soonbin", "Chung", 126620014, "320 Riverside Dr Apt 11");
            users.add(user);
            user = new Users("Paul", "Zimmerman", 126620049, "250 W 104th St Apt");
            users.add(user);
            user = new Users("Benjamin", "Goldstein", 126620049, "250 W 104th St");
            users.add(user);
            user = new Users("Okanlawo", "Coker", 324483068, "173 Minetta Rd Somerset");
            users.add(user);
            user = new Users("Joe", "Berger", 124480287, "400 W 43rd St Apt 33p");
            users.add(user);
            user = new Users("Joseph", "Carter", 138954300, "2431 W Grand Blvd");
            users.add(user);
            user = new Users("Zeena", "Kazangy", 138411010, "6061 W Vernor Hwy Ste 1");
            users.add(user);
            user = new Users("Koepp", "Norman", 138715489, "3011 W Grand Blvd Ste 871");
            users.add(user);
            user = new Users("Garner", "Toya", 138397900, "14521 E 7 Mile Rd");
            users.add(user);
            user = new Users("Kim", "Fleming", 138918088, "3704 E 8 Mile Rd");
            users.add(user);
            user = new Users("Nadell", "Sergent", 138918088, "3704 E 8 Mile Rd");
            users.add(user);
            user = new Users("Ajay", "Ojha", 126220086, "2700 Martin Luther King Jr Blvd");
            users.add(user);
            user = new Users("Jeffery", "Jaskolski", 126630086, "22151 Moross Rd Ste G34");
            users.add(user);
            user = new Users("Michael", "Szewczyk", 138424255, "7433 Michigan Ave");
            users.add(user);
            user = new Users("Malaney", "Brown", 138384880, "14755 Fenkell St");
            users.add(user);
            user = new Users("Robert", "Roy", 132715555, "19600 W Warren Ave");
            users.add(user);
            user = new Users("Jordan", "Millard", 132720118, "17563 Greenfield Rd Ste 1");
            users.add(user);
            user = new Users("Alan", "Josef", 138397310, "19150 Kelly Rd");
            users.add(user);
            user = new Users("Milton", "Campbell", 226620086, "7142 W 7 Mile Rd");
            users.add(user);
            user = new Users("Dennis", "Rogers", 135334333, "20429 W 7 Mile Rd");
            users.add(user);
            user = new Users("Gabriel", "Williams", 138737474, "3011 W Grand Blvd Ste 867");
            users.add(user);
            for (Users sUser : users) {
                userRepository.save(sUser);
            }
        }
}

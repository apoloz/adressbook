package ua.poloz.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.poloz.model.Users;

public interface UserRepository extends JpaRepository<Users, Integer>{

}

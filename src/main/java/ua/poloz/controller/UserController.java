package ua.poloz.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ua.poloz.model.Users;
import ua.poloz.service.UserService;

@Controller
@RequestMapping(value="/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView createUserPage() {
		ModelAndView mav = new ModelAndView("users/new-user");
		mav.addObject("sUser", new Users());
		return mav;
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Users createUser(@RequestBody Users user) {
		return userService.create(user);
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editUserPage(@PathVariable int id) {
		ModelAndView mav = new ModelAndView("users/edit-user");
		Users user = userService.get(id);
		mav.addObject("sUser", user);
		return mav;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.PUT, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Users editUser(@PathVariable int id, 
			@RequestBody Users user) {
		user.setId(id);
		return userService.update(user);
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Users deleteUser(@PathVariable int id) {
		return userService.delete(id);
	}
	
	@RequestMapping(value="", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Users> allUsers() {
		return userService.getAll();
	}
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ModelAndView allUsersPage() {
		ModelAndView mav = new ModelAndView("users/all-users");
		List<Users> users = new ArrayList<Users>();
		users.addAll(allUsers());
		mav.addObject("users", users);
		return mav;
	}
	
        @RequestMapping(value="/init", method=RequestMethod.GET)
        public ModelAndView initTestUsers(){
            System.out.println("initTestUsers");
            userService.initTestUsers();
            ModelAndView mav = new ModelAndView("users/init");
//            List<Users> users = new ArrayList<Users>();
//		users.addAll(allUsers());
//		mav.addObject("users", users);
		return mav;
        }
}

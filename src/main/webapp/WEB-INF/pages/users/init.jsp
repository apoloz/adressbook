<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Home page</title>
<link href="resources/css/main.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="container">
<h1>Test Users create!</h1>
<p>
<a href="${pageContext.request.contextPath}/users.html">All Users</a><br/>
</p>
</div>
</body>
</html>
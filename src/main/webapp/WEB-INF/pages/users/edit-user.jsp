<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Edit User</title>
        <!--<link href="../../resources/css/main.css" rel="stylesheet" type="text/css"/>-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function () {

                $('#editUserForm').submit(function (event) {

                    var firstName = $('#firstName').val();
                    var lastName = $('#lastName').val();
                    var phoneNumber = $('#phoneNumber').val();
                    var address = $('#address').val();
                    var json = {"firstName": firstName, "lastName": lastName, "phoneNumber": phoneNumber, "address": address};

                    $.ajax({
                        url: $("#editUserForm").attr("action"),
                        data: JSON.stringify(json),
                        type: "PUT",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function (user) {
                            var respContent = "";

                            respContent += "<span class='success'>User was edited: [";
                            respContent += user.firstName + " : ";
                            respContent += user.lastName + " : ";
                            respContent += user.phoneNumber + " : ";
                            respContent += user.address + "]</span>";

                            $("#sUserFromResponse").html(respContent);
                        }
                    });

                    event.preventDefault();
                });

            });
        </script>

    </head>
    <body>
        <div id="container">
            <h1>Edit User</h1>
            <div id="sUserFromResponse">
                <p>Here you can edit User info:</p>
                <div id="sUserFromResponse"></div>
            </div>
            <form:form id="editUserForm" method="PUT" commandName="sUser" 
                       action="${pageContext.request.contextPath}/users/edit/${sUser.id}.json">
                <table>
                    <tbody>
                        <tr>
                            <td>First Name:</td>
                            <td><form:input path="firstName" /></td>
                        </tr>
                        <tr>
                            <td>Last Name:</td>
                            <td><form:input path="lastName" /></td>
                        </tr>
                        <tr>
                            <td>Phone Number:</td>
                            <td><form:input path="phoneNumber" /></td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td><form:input path="address" /></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Edit" /></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </form:form>
            <a href="${pageContext.request.contextPath}/index.html">Home page</a><br/>
            <a href="${pageContext.request.contextPath}/users.html">All Users</a><br/>
        </div>
    </body>
</html>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>All Users</title>
        <script src = "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>

    </head>
    <body>
        <div id="container">
            <h1>All Users</h1>
            <div>
                <p>Here you can see a list of Users:</p>
                <div id="sUserFromResponse"></div>
            </div>
            <input type="search" class="light-table-filter" data-table="userTable" placeholder="Filter"></input>
            <p>The number of records per page 
                <select class="entries" >
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                </select></p>
            <table class="userTable" border="1px" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>First Name</th><th>Last Name</th><th>Phone Number</th><th>Address</th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="sUser" items="${users}">
                        <tr>
                            <td>${sUser.firstName}</td>
                            <td>${sUser.lastName}</td>
                            <td>${sUser.phoneNumber}</td>
                            <td>${sUser.address}</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/users/edit/${sUser.id}.html">Edit</a><br/>
                                <a href="${pageContext.request.contextPath}/users/delete/${sUser.id}.json">Delete</a><br/>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <a href="${pageContext.request.contextPath}/index.html">Home page</a><br/>
            <a href="${pageContext.request.contextPath}/users/create.html">Create new User</a><br/>
        </div>
        <script type="text/javascript">

            $('.userTable').each(function () {
//                         $.ajax({
                 var currentPage = 0;
        var numPerPage = $('.entries').val();
        var $table = $(this);

        $table.bind('repaginate', function () {

            numPerPage = $('.entries').val();
            numPages = Math.ceil(numRows / numPerPage);

            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();

        });

        //$table.trigger('repaginate');

        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<ul class="pagination"></ul>');
        var $previous = $('<li><a href="#">&laquo;</a></li>');
        var $next = $('<li><a href="#">&raquo;</a></li>');

        var currentAnchorsShow = function() {
            $pager.insertBefore($table).find('span.page-number:first').addClass('active');
            $previous.insertBefore('span.page-number:first');
            $next.insertAfter('span.page-number:last');

            $next.click(function (e) {
                $previous.addClass('clickable');
                $pager.find('.active').next('.page-number.clickable').click();
            });

            $previous.click(function (e) {
                $next.addClass('clickable');
                $pager.find('.active').prev('.page-number.clickable').click();
            });
        };

        var anchorsPagesAdd = function() {
            for (var page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>').text(page + 1).bind('click', {
                    newPage: page
                }, function (event) {

                    currentPage = event.data['newPage'];

                    $table.trigger('repaginate');

                    $(this).addClass('active').siblings().removeClass('active');
                }).appendTo($pager).addClass('clickable');
            }

            currentAnchorsShow();
        }

        var anchorsPagesRemove = function(){
            $( ".page-number" ).remove();
        };

        anchorsPagesAdd();

        $table.on('repaginate', function () {

            $next.addClass('clickable');
            $previous.addClass('clickable');

            setTimeout(function () {
                var $active = $pager.find('.page-number.active');
                if ($active.next('.page-number.clickable').length === 0) {
                    $next.removeClass('clickable');
                } else if ($active.prev('.page-number.clickable').length === 0) {
                    $previous.removeClass('clickable');
                }
            });
        });

        $('.entries').on('change', function() {
            $table.trigger('repaginate');
            anchorsPagesRemove();
            anchorsPagesAdd();
        })

        $table.trigger('repaginate');
//                    });
    });</script>
        <script type="text/javascript">
            $(document).ready(function () {
                var deleteLink = $("a:contains('Delete')");
                $(deleteLink).click(function (event) {

                    $.ajax({
                        url: $(event.target).attr("href"),
                        type: "DELETE",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function (user) {
                            var respContent = "";
                            var rowToDelete = $(event.target).closest("tr");
                            rowToDelete.remove();
                            respContent += "<span class='success'>User was deleted: [";
                            respContent += user.producer + " : ";
                            respContent += user.model + " : ";
                            respContent += user.price + "]</span>";
                            $("#sUserFromResponse").html(respContent);
                        }
                    });
                    event.preventDefault();
                });
            });</script>
        <script type="text/javascript">
            (function (document) {
                'use strict';
                var LightTableFilter = (function (Arr) {

                    var _input;
                    function _onInputEvent(e) {
                        _input = e.target;
                        var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
                        Arr.forEach.call(tables, function (table) {
                            Arr.forEach.call(table.tBodies, function (tbody) {
                                Arr.forEach.call(tbody.rows, _filter);
                            });
                        });
                    }

                    function _filter(row) {
                        var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
                        row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
                    }

                    return {
                        init: function () {
                            var inputs = document.getElementsByClassName('light-table-filter');
                            Arr.forEach.call(inputs, function (input) {
                                input.oninput = _onInputEvent;
                            });
                        }
                    };
                })(Array.prototype);
                document.addEventListener('readystatechange', function () {
                    if (document.readyState === 'complete') {
                        LightTableFilter.init();
                    }
                });
            })(document);</script>
        <script type="text/javascript">
            (function ($) {
                $.fn.extend({
                    tableAddCounter: function (options) {

                        // set up default options 
                        var defaults = {
                            title: '#',
                            start: 1,
                            id: false,
                            class: false,
                        };
                        // Overwrite default options with user provided
                        var options = $.extend({}, defaults, options);
                        return $(this).each(function () {
                            // Make sure this is a table tag
                            if ($(this).is('table')) {

                                // Add column title unless set to 'false'
                                if (!options.title)
                                    options.title = '';
                                $('th:first-child, thead td:first-child', this).each(function () {
                                    var tagName = $(this).prop('tagName');
                                    $(this).before('<' + tagName + ' rowspan="' + $('thead tr').length + '" class="' + options.class + '" id="' + options.id + '">' + options.title + '</' + tagName + '>');
                                });
                                // Add counter starting counter from 'start'
                                $('tbody td:first-child', this).each(function (i) {
                                    $(this).before('<td>' + (options.start + i) + '</td>');
                                });
                            }
                        });
                    }
                });
            })(jQuery);
            var tr = document.getElementsByTagName('table').item(0).getElementsByTagName('tr').length;
            if (tr > 25) {
                var options = {
                    name: "Count",
                    class: "counter",
//                    start: 26
                };
                $('.userTable').tableAddCounter(options);
            }
        </script>
    </body>
</html>